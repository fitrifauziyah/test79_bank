"""
URL configuration for bank project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from bankApp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('customers/', views.CustomerList.as_view(), name='customer-list'),
    path('customers/<int:pk>/', views.CustomerDetail.as_view(), name='customer-detail'),
    path('customers/add/', views.CustomerAdd.as_view(), name='customer-add'),
    path('transactions/', views.TransactionList.as_view(), name='transaction-list'),
    path('transactions/add/', views.TransactionAdd.as_view(), name='add-transaction'),
    path('customers/point-list/', views.point_list, name='point-list'),
    path('transaction/report/', views.transaction_report, name='transaction-report'),
]
