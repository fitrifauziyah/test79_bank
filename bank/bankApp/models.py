from django.db import models
from datetime import datetime

class Customer(models.Model):
    account_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False)

    def __str__(self):
        return self.name

class TransactionType(models.Model):
    Transaction_type_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    # transaction_category = models.CharField(max_length=6, choices=[('D', 'Debit'), ('C', 'Credit')])

    def __str__(self):
        return self.name

class Transaction(models.Model):
    transaction_id = models.AutoField(primary_key=True)
    account = models.ForeignKey(Customer, on_delete=models.CASCADE)
    transaction_date = models.DateTimeField(default=datetime.now)
    description = models.ForeignKey(TransactionType, on_delete=models.CASCADE)
    debit_credit_status = models.CharField(max_length=1, choices=[('C', 'Credit'), ('D', 'Debit')])
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    points = models.IntegerField(default=0)

