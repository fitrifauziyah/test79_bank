from django import forms
from .models import Customer, Transaction, TransactionType

class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ['name']

class TransactionTypeChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name

class TransactionForm(forms.ModelForm):
    description = TransactionTypeChoiceField(queryset=TransactionType.objects.all())

    class Meta:
        model = Transaction
        fields = ['account', 'transaction_date', 'description', 'debit_credit_status', 'amount']
        widgets = {
            'account': forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['account'].queryset = Customer.objects.only('account_id', 'name')
        self.fields['account'].to_field_name = 'account_id'

class TransactionReportForm(forms.Form):
    account_id = forms.IntegerField()
    start_date = forms.DateField()
    end_date = forms.DateField()