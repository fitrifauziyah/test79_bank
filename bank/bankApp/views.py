from rest_framework import generics
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.views.generic import ListView
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.db.models import DateField

from .models import Customer, Transaction
from .serializers import CustomerSerializer
from .forms import CustomerForm, TransactionForm, TransactionReportForm

# class CustomerList(generics.ListCreateAPIView):
#     queryset = Customer.objects.all()
#     serializer_class = CustomerSerializer

# class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Customer.objects.all()
#     serializer_class = CustomerSerializer

class CustomerList(CreateView):
    template_name = 'customer_list.html'
    model = Customer
    fields = ['name']
    success_url = reverse_lazy('customer-list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['customers'] = Customer.objects.all()
        return context

class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

class CustomerAdd(CreateView):
    template_name = 'customer_add.html'
    form_class = CustomerForm
    success_url = reverse_lazy('customer-list')
    
class TransactionAdd(CreateView):
    template_name = 'add_transaction.html'
    form_class = TransactionForm
    success_url = reverse_lazy('transaction-list')

    def calculate_points(self, form):
        transaction = form.save(commit=False)
        points = 0

        if transaction.description.name == 'Beli Pulsa':
            if transaction.amount <= 10000:
                points = 0
            elif 10000 < transaction.amount <= 30000:
                points = (transaction.amount - 10000) / 1000
            else:
                points = 20 + ((transaction.amount - 30000) / 1000) * 2
        elif transaction.description.name == 'Bayar Listrik':
            if transaction.amount <= 50000:
                points = 0
            elif 50001 <= transaction.amount <= 100000:
                points = (transaction.amount - 50000) / 2000
            else:
                points = 25 + ((transaction.amount - 100000) / 2000) * 2

        transaction.points = points
        transaction.save()
        return transaction

    def form_valid(self, form):
        transaction = self.calculate_points(form)
        return super().form_valid(form)

class TransactionList(ListView):
    template_name = 'transaction_list.html'
    model = Transaction
    context_object_name = 'transactions'

def point_list(request):
    customers = Customer.objects.all()
    point_data = []

    for customer in customers:
        total_points = Transaction.objects.filter(account=customer).aggregate(total_points=Coalesce(Sum('points'), 0))['total_points']
        point_data.append({
            'account_id': customer.account_id,
            'name': customer.name,
            'total_points': total_points,
        })

    return render(request, 'customer_points.html', {'point_data': point_data})

def transaction_report(request):
    if request.method == 'POST':
        form = TransactionReportForm(request.POST)
        if form.is_valid():
            account_id = form.cleaned_data['account_id']
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']

            transactions = Transaction.objects.filter(
                account_id=account_id,
                transaction_date__date__gte=start_date,
                transaction_date__date__lte=end_date
            ).order_by('transaction_date')

            return render(request, 'transaction_report.html', {'transactions': transactions})
    else:
        form = TransactionReportForm()

    return render(request, 'transaction_report_form.html', {'form': form})
