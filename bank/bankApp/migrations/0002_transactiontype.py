# Generated by Django 4.2 on 2023-09-18 06:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bankApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionType',
            fields=[
                ('Transaction_type_id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
            ],
        ),
    ]
